//Criando function findLyrics para consumir a API
const findLyrics = (artist, title) => {
  return fetch(`https://api.lyrics.ovh/v1/${artist}/${title}`);
};

//criando const para utilizar de itens do form
const form = document.querySelector("#lyrics_form");
//function para cancelar default submit e chamar novo submit
form.addEventListener("submit", (el) => {
  //cancelando o comportamento default(submit)
  el.preventDefault();
  doSubmit();
});

//Criando novo submit
const doSubmit = async () => {
  const artist = document.querySelector("#artist").value;
  const song = document.querySelector("#song").value;
  const lyrics_el = document.querySelector("#lyrics");

  //div com class spinner-grow para animação de Carregar
  lyrics_el.innerHTML =
    "<div class='spinner-grow text-primary' style='width: 3rem; height: 3rem;' role='status'><span class='sr-only'>Loading...</span></div>";

  try {
    const lyricsReponse = await findLyrics(artist, song);
    const data = await lyricsReponse.json();
    if (lyricsReponse.status == 200) {
      lyrics_el.innerHTML = data.lyrics;
    } else {
      lyrics_el.innerHTML = data.error;
    }
  } catch (error) {
    console.log(error);
  }
};
