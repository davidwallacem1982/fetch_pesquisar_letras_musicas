# Fetch_Pesquisar_Letras_Musicas

# Projeto Padrão em Html e javascript

Esse projeto foi feito para estudo do fetch no javascript

## Objetivo

Criar uma estrutura para estudos do novo projeto que será criado para utilizar técnicas de fetch utilizando functions `async await`.

### `Fetch API`

A Fetch API fornece uma interface para buscar recursos (por exemplo, em toda a rede). Parecerá familiar para qualquer pessoa que tenha usado XMLHttpRequest, porém a nova API oferece um conjunto de recursos mais poderoso e flexível.
Mais informações entre em [https://developer.mozilla.org/pt-BR/docs/Web/API/Fetch_API](https://developer.mozilla.org/pt-BR/docs/Web/API/Fetch_API).
